//
//  XXXEquivalenceUtilityFunctions.m
//  XXXCoreUtilities
//

#import "XXXEquivalenceUtilityFunctions.h"


BOOL XXXEquivalentObjects(id lhs, id rhs) {
  return ((lhs == rhs)
          || ((lhs != nil)
              && (rhs != nil)
              && ([lhs isEqual:rhs])));
}
