# Star Rating View ★★★☆☆ #

##Custom view for ratings similar to the stars found in the AppStore. ##

* The view is *IB_DESIGNABLE* and allows you to configure which images to use. It assumes they're the same size. 
* It also allows you to configure the color of the stars on normal and highlighted state.
* There are two style options:
    * continuous (starts try to display accurate values (e.g. if it's 4.6, then the last star is ~".6 full"))  
    * discrete (Stars only display three values: empty, full, and half-full). In discrete mode you can configure the minimum and maximum score an individual star needs to gain a "half-full state".

To see how it works open the workspace and then in the XXXCoreInterface project look for the TestStarView.xib under XXXCoreInterface/StarView.  Select the star view and change the values in the _Attributes Inspector_