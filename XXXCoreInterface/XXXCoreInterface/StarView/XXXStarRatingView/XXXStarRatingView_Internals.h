//
//  XXXStarRatingView_Internals.h
//  XXXCoreInterface
//

#import "XXXStarRatingView.h"

@interface XXXStarRatingView ()

@property (nonatomic, assign) IBInspectable CGFloat inspectableRating;

// -------------------------------------------------------------------------- //
/*!
 * @methodgroup Display-Style Properties - Layout
 */
// -------------------------------------------------------------------------- //

/*! Insets that "pad out" the area around the star views. */
@property (assign, nonatomic) IBInspectable CGFloat topStarInset;
@property (assign, nonatomic) IBInspectable CGFloat leftStarInset;
@property (assign, nonatomic) IBInspectable CGFloat bottomStarInset;
@property (assign, nonatomic) IBInspectable CGFloat rightStarInset;

/*! Amount of horizontal spacing from star-to-star. */
@property (assign, nonatomic) IBInspectable CGFloat starToStarGap;

// -------------------------------------------------------------------------- //
/*!
 * @methodgroup Display-Style Properties - Behavior
 */
// -------------------------------------------------------------------------- //

/*! The display style it should use for showing its scores. */
@property (assign, nonatomic) IBInspectable XXXStarRatingViewStyle displayStyle;

/*! A value in [0.0, 1.0]. In the "discrete" mode, this is the minimum score an individual star needs to go to the "half-full state". */
@property (assign, nonatomic) IBInspectable CGFloat minimumHalfFullAmount;

/*! A value in [0.0, 1.0]. In the "discrete" mode, this is the maximum score for which an individual star is in the "half-full state". */
@property (assign, nonatomic) IBInspectable CGFloat maximumHalfFullAmount;

// -------------------------------------------------------------------------- //
/*!
 * @methodgroup Display-Style Properties - Colors
 */
// -------------------------------------------------------------------------- //

/*! Color to use to tint the filled star images in a normal state. */
@property (strong, nonatomic) IBInspectable UIColor *filledStarColor;

/*! Color to use to tint the filled star images in a highlighted state. */
@property (strong, nonatomic) IBInspectable UIColor *filledStarHighlightColor;

/*! Color to use to tint the empty star images in a normal state. */
@property (strong, nonatomic) IBInspectable UIColor *emptyStarColor;

/*! Color to use to tint the empty star images in a highlighted state. */
@property (strong, nonatomic) IBInspectable UIColor *emptyStarHighlightColor;

// -------------------------------------------------------------------------- //
/*!
 * @methodgroup Display-Style Properties - Images
 */
// -------------------------------------------------------------------------- //

/*! Image to use for the filled stars in a normal state. */
@property (strong, nonatomic) IBInspectable UIImage *filledStarImage;

/*! Image to use for the filled stars in a highlighted state. */
@property (strong, nonatomic) IBInspectable UIImage *filledStarHighlightImage;

/*! Image to use for the empty stars in a normal state. */
@property (strong, nonatomic) IBInspectable UIImage *emptyStarImage;

/*! Image to use for the empty stars in a highlighted state. */
@property (strong, nonatomic) IBInspectable UIImage *emptyStarHighlightImage;

@end

