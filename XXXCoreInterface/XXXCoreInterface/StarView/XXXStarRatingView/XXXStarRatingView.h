//
//  XXXStarRatingView.h
//  XXXCoreInterface
//

@import UIKit;
#import "XXXStarRatingViewProtocols.h"

IB_DESIGNABLE
@interface XXXStarRatingView : UIView

@property (assign, nonatomic, getter=isHighlighted) BOOL highlighted;

/**
 *  Current rating. From [0.0, 5.0]
 */
@property (nonatomic, assign) CGFloat currentRating;

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithFrame:(CGRect)frame NS_UNAVAILABLE;

- (instancetype)initWithFrame:(CGRect)frame
             layoutParameters:(id<XXXStarLayoutParameters>)layoutParameters
                 displayStyle:(id<XXXStarDisplayStyle>)displayStyle
                   colorStyle:(id<XXXStarColorStyle>)colorStyle
                   imageGroup:(id<XXXStarImageGroup>)imageGroup NS_DESIGNATED_INITIALIZER;


- (void)startUsingLayoutParameters:(id<XXXStarLayoutParameters>)layoutParameters;

- (void)startUsingDisplayStyle:(id<XXXStarDisplayStyle>)displayStyle;

- (void)startUsingColorStyle:(id<XXXStarColorStyle>)colorStyle;

- (void)startUsingImageGroup:(id<XXXStarImageGroup>)imageGroup;

@end
