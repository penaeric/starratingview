//
//  Header.h
//  XXXCoreInterface
//

typedef NS_ENUM(NSInteger, XXXStarRatingViewStyle) {
    /**
     *  Starts try to display accurate values (e.g. if it's 4.6, then the last star is ~".6 full").
     */
    XXXStarRatingViewStyleContinuous = 1,
    /**
     *  Stars only display thre values: empty, full, and half-full.
     */
    XXXStarRatingViewStyleDiscrete = 2
};
