//
//  XXXStarRatingView.m
//  XXXCoreInterface
//

@import XXXCoreUtilities;
#import "XXXStarRatingView_Internals.h"
#import "XXXStarRatingStarView_Internals.h"

@interface XXXStarRatingView ()

@property (strong, nonatomic) XXXStarRatingStarView *starView1;
@property (strong, nonatomic) XXXStarRatingStarView *starView2;
@property (strong, nonatomic) XXXStarRatingStarView *starView3;
@property (strong, nonatomic) XXXStarRatingStarView *starView4;
@property (strong, nonatomic) XXXStarRatingStarView *starView5;
@property (strong, nonatomic) NSArray *starViews;

@property (assign, nonatomic) CGFloat currentStarToStarGap;
@property (assign, nonatomic) CGSize contentSize;
@property (assign, nonatomic) UIEdgeInsets starAreaInsets;

@end

@implementation XXXStarRatingView

#pragma mark - Init

- (instancetype)initWithFrame:(CGRect)frame {
    return [self initWithFrame:frame
              layoutParameters:nil
                  displayStyle:nil
                    colorStyle:nil
                    imageGroup:nil];
}

- (instancetype)initWithFrame:(CGRect)frame
             layoutParameters:(id<XXXStarLayoutParameters>)layoutParameters
                 displayStyle:(id<XXXStarDisplayStyle>)displayStyle
                   colorStyle:(id<XXXStarColorStyle>)colorStyle
                   imageGroup:(id<XXXStarImageGroup>)imageGroup {
    if (self == [super initWithFrame:frame]) {
        [self setupViews];
        [self startUsingLayoutParameters:layoutParameters];
        [self startUsingDisplayStyle:displayStyle];
        [self startUsingColorStyle:colorStyle];
        [self startUsingImageGroup:imageGroup];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupViews];
}

#pragma mark - Setup

- (void)setupViews {
    if (self.starView1 == nil) {
        self.starAreaInsets = UIEdgeInsetsZero;
        self.starToStarGap = 0.0;
        self.starView1 = [[XXXStarRatingStarView alloc] init];
        self.starView2 = [[XXXStarRatingStarView alloc] init];
        self.starView3 = [[XXXStarRatingStarView alloc] init];
        self.starView4 = [[XXXStarRatingStarView alloc] init];
        self.starView5 = [[XXXStarRatingStarView alloc] init];
        self.starView1.translatesAutoresizingMaskIntoConstraints = NO;
        self.starView2.translatesAutoresizingMaskIntoConstraints = NO;
        self.starView3.translatesAutoresizingMaskIntoConstraints = NO;
        self.starView4.translatesAutoresizingMaskIntoConstraints = NO;
        self.starView5.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.starView1];
        [self addSubview:self.starView2];
        [self addSubview:self.starView3];
        [self addSubview:self.starView4];
        [self addSubview:self.starView5];
        
        self.starViews = @[self.starView1, self.starView2, self.starView3, self.starView4, self.starView5];
    }
}

#pragma mark - Public API

- (void)setCurrentRating:(CGFloat)currentRating {
    _currentRating = currentRating;
    [self.starView1 setRating:[self ratingForStarNumber:1.0]];
    [self.starView2 setRating:[self ratingForStarNumber:2.0]];
    [self.starView3 setRating:[self ratingForStarNumber:3.0]];
    [self.starView4 setRating:[self ratingForStarNumber:4.0]];
    [self.starView5 setRating:[self ratingForStarNumber:5.0]];
}

- (void)setHighlighted:(BOOL)highlighted {
    if (highlighted != self.isHighlighted) {
        _highlighted = highlighted;
        for (XXXStarRatingStarView *starView in self.starViews) {
            [starView setHighlighted:highlighted];
        }
    }
}

- (void)startUsingLayoutParameters:(id<XXXStarLayoutParameters>)layoutParameters {
    BOOL needsUpdate = NO;
    if (!UIEdgeInsetsEqualToEdgeInsets(layoutParameters.starAreaInsets, self.starAreaInsets)) {
        self.starAreaInsets = layoutParameters.starAreaInsets;
        needsUpdate = YES;
    }
    if (layoutParameters.starToStarGap != self.currentStarToStarGap) {
        self.currentStarToStarGap = layoutParameters.starToStarGap;
        needsUpdate = YES;
    }
    if (needsUpdate) {
        [self setNeedsLayout];
    }
}

- (void)startUsingDisplayStyle:(id<XXXStarDisplayStyle>)displayStyle {
    if (displayStyle.viewStyle != self.displayStyle) {
        for (XXXStarRatingStarView *starView in self.starViews) {
            starView.starViewStyle = displayStyle.viewStyle;
        }
    }
    
    if (displayStyle.minimumHalfFullAmount != self.minimumHalfFullAmount) {
        self.minimumHalfFullAmount = displayStyle.minimumHalfFullAmount;
    }
    
    if (displayStyle.maximumHalfFullAmount != self.maximumHalfFullAmount) {
        self.maximumHalfFullAmount = displayStyle.maximumHalfFullAmount;;
    }
}

- (void)startUsingColorStyle:(id<XXXStarColorStyle>)colorStyle {
    self.filledStarColor = colorStyle.filledStarColor;
    self.filledStarHighlightColor = colorStyle.filledStarHighlightColor;
    self.emptyStarColor = colorStyle.emptyStarColor;
    self.emptyStarHighlightColor = colorStyle.emptyStarHighlightColor;
}

- (void)startUsingImageGroup:(id<XXXStarImageGroup>)imageGroup {
    self.filledStarImage = imageGroup.filledStarImage;
    self.filledStarHighlightImage = imageGroup.filledStarHighlightImage;
    self.emptyStarImage = imageGroup.emptyStarImage;
    self.emptyStarHighlightImage = imageGroup.emptyStarHighlightImage;
}

#pragma mark - Inspectable Accessors

- (void)setInspectableRating:(CGFloat)inspectableRating {
    if (inspectableRating != self.currentRating) {
        self.currentRating = inspectableRating;
    }
}

- (void)setStarToStarGap:(CGFloat)starToStarGap {
    if (starToStarGap != self.currentStarToStarGap) {
        self.currentStarToStarGap = starToStarGap;
        [self setNeedsLayout];
    }
}

@dynamic displayStyle;
- (XXXStarRatingViewStyle)displayStyle {
    return self.starView1.starViewStyle;
}

@dynamic minimumHalfFullAmount;
- (CGFloat)minimumHalfFullAmount {
    return self.starView1.minimumHalfFullAmount;
}

- (void)setMinimumHalfFullAmount:(CGFloat)minimumHalfFullAmount {
    if (minimumHalfFullAmount != self.minimumHalfFullAmount) {
        for (XXXStarRatingStarView *starView in self.starViews) {
            starView.minimumHalfFullAmount = minimumHalfFullAmount;
        }
    }
}

@dynamic maximumHalfFullAmount;
- (CGFloat)maximumHalfFullAmount {
    return self.starView1.maximumHalfFullAmount;
}

- (void)setMaximumHalfFullAmount:(CGFloat)maximumHalfFullAmount {
    if (maximumHalfFullAmount != self.maximumHalfFullAmount) {
        for (XXXStarRatingStarView *starView in self.starViews) {
            starView.maximumHalfFullAmount = maximumHalfFullAmount;
        }
    }
}

#pragma mark Inspectable Accessors - Color

@dynamic filledStarColor;
- (UIColor *)filledStarColor {
    return self.starView1.filledStarColor;
}

- (void)setFilledStarColor:(UIColor *)filledStarColor {
    if (!XXXEquivalentObjects(filledStarColor, self.filledStarColor)) {
        for (XXXStarRatingStarView *starView in self.starViews) {
            starView.filledStarColor = filledStarColor;
        }
    }
}

@dynamic filledStarHighlightColor;
- (UIColor *)filledStarHighlightColor {
    return self.starView1.filledStarHighlightColor;
}

- (void)setFilledStarHighlightColor:(UIColor *)filledStarHighlightColor {
    if (!XXXEquivalentObjects(filledStarHighlightColor, self.filledStarHighlightColor)) {
        for (XXXStarRatingStarView *starView in self.starViews) {
            starView.filledStarHighlightColor = filledStarHighlightColor;
        }
    }
}

@dynamic emptyStarColor;
- (UIColor *)emptyStarColor {
    return self.starView1.emptyStarColor;
}

- (void)setEmptyStarColor:(UIColor *)emptyStarColor {
    if (!XXXEquivalentObjects(emptyStarColor, self.emptyStarColor)) {
        for (XXXStarRatingStarView *starView in self.starViews) {
            starView.emptyStarColor = emptyStarColor;
        }
    }
}

@dynamic emptyStarHighlightColor;
- (UIColor *)emptyStarHighlightColor {
    return self.starView1.emptyStarHighlightColor;
}

- (void)setEmptyStarHighlightColor:(UIColor *)emptyStarHighlightColor {
    if (!XXXEquivalentObjects(emptyStarHighlightColor, self.emptyStarHighlightColor)) {
        for (XXXStarRatingStarView *starView in self.starViews) {
            starView.emptyStarHighlightColor = emptyStarHighlightColor;
        }
    }
}

#pragma mark Inspectable Accessors - Images

@dynamic filledStarImage;
- (UIImage *)filledStarImage {
    return self.starView1.filledStarImage;
}

- (void)setFilledStarImage:(UIImage *)filledStarImage {
    for (XXXStarRatingStarView *starView in self.starViews) {
        starView.filledStarImage = [filledStarImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
}

@dynamic filledStarHighlightImage;
- (UIImage *)filledStarHighlightImage {
    return self.starView1.filledStarHighlightImage;
}

- (void)setFilledStarHighlightImage:(UIImage *)filledStarHighlightImage {
    for (XXXStarRatingStarView *starView in self.starViews) {
        starView.filledStarHighlightImage = [filledStarHighlightImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
}

@dynamic emptyStarImage;
- (UIImage *)emptyStarImage {
    return self.starView1.emptyStarImage;
}

- (void)setEmptyStarImage:(UIImage *)emptyStarImage {
    for (XXXStarRatingStarView *starView in self.starViews) {
        starView.emptyStarImage = [emptyStarImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
}

@dynamic emptyStarHighlightImage;
- (UIImage *)emptyStarHighlightImage {
    return self.starView1.emptyStarHighlightImage;
}

- (void)setEmptyStarHighlightImage:(UIImage *)emptyStarHighlightImage {
    for (XXXStarRatingStarView *starView in self.starViews) {
        starView.emptyStarHighlightImage = [emptyStarHighlightImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
}

#pragma mark Inspectable Accessors - Star Gaps

- (void)setTopStarInset:(CGFloat)topStarInset {
    if (topStarInset != self.starAreaInsets.top) {
        self.starAreaInsets = UIEdgeInsetsMake(topStarInset, self.starAreaInsets.left, self.starAreaInsets.bottom, self.starAreaInsets.right);
        [self setNeedsLayout];
    }
}

- (void)setLeftStarInset:(CGFloat)leftStarInset {
    if (leftStarInset != self.starAreaInsets.left) {
        self.starAreaInsets = UIEdgeInsetsMake(self.starAreaInsets.top, leftStarInset, self.starAreaInsets.bottom, self.starAreaInsets.right);
        [self setNeedsLayout];
    }
}

- (void)setBottomStarInset:(CGFloat)bottomStarInset {
    if (bottomStarInset != self.starAreaInsets.bottom) {
        self.starAreaInsets = UIEdgeInsetsMake(self.starAreaInsets.top, self.starAreaInsets.left, bottomStarInset, self.starAreaInsets.right);
        [self setNeedsLayout];
    }
}

- (void)setRightStarInset:(CGFloat)rightStarInset {
    if (rightStarInset != self.starAreaInsets.right) {
        self.starAreaInsets = UIEdgeInsetsMake(self.starAreaInsets.top, self.starAreaInsets.left, self.starAreaInsets.bottom, rightStarInset);
        [self setNeedsLayout];
    }
}

#pragma mark -

- (CGSize)intrinsicContentSize {
    return self.contentSize;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    UIImage *image = self.filledStarImage ?: self.emptyStarImage;
    CGFloat imageWidth = image.size.width;
    CGFloat imageHeight = image.size.height;
    UIEdgeInsets starInsets = self.starAreaInsets;
    CGFloat x = starInsets.left;
    CGFloat y = starInsets.top;
    CGFloat gap = self.currentStarToStarGap;
    
    self.starView1.frame = CGRectMake(x, y, imageWidth, imageHeight);
    x += imageWidth + gap + starInsets.left + starInsets.right;
    self.starView2.frame = CGRectMake(x, y, imageWidth, imageHeight);
    x += imageWidth + gap + starInsets.left + starInsets.right;
    self.starView3.frame = CGRectMake(x, y, imageWidth, imageHeight);
    x += imageWidth + gap + starInsets.left + starInsets.right;
    self.starView4.frame = CGRectMake(x, y, imageWidth, imageHeight);
    x += imageWidth + gap + starInsets.left + starInsets.right;
    self.starView5.frame = CGRectMake(x, y, imageWidth, imageHeight);
    
    x += imageWidth + starInsets.right;
    CGSize currentSize = self.contentSize;
    CGSize newSize = CGSizeMake(x, imageHeight + starInsets.top + starInsets.bottom);
    if (!CGSizeEqualToSize(currentSize, newSize)) {
        self.contentSize = newSize;
        [self invalidateIntrinsicContentSize];
    }
}

- (CGFloat)ratingForStarNumber:(CGFloat)starNumber {
    CGFloat rating = 0;
    
    if (starNumber <= self.currentRating) {
        rating = 1.0;
    } else if ((starNumber -1) < self.currentRating) {
        rating = fmod(self.currentRating, 1.0);
    }
    
    return rating;
}

@end
