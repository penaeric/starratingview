//
//  XXXStarRatingViewProtocols.h
//  XXXCoreInterface
//

@import Foundation;
#import "XXXStarRatingViewStyle.h"

@protocol XXXStarLayoutParameters <NSObject>

/**
 *  Insets that "pad out" the area around the star views.
 */
@property (assign, nonatomic) UIEdgeInsets starAreaInsets;

/**
 *  Amount of horizontal spacing from star-to-star.
 */
@property (assign, nonatomic) CGFloat starToStarGap;

@end


@protocol XXXStarDisplayStyle <NSObject>

/**
 *  The display style it should use for showing its scores.
 */
@property (assign, nonatomic) XXXStarRatingViewStyle viewStyle;
/**
 *  A value in [0.0, 1.0]. In the "discrete" mode, this is the minimum score an individual star needs to go to the "half-full state".
 */
@property (assign, nonatomic) CGFloat minimumHalfFullAmount;
/**
 *  A value in [0.0, 1.0]. In the "discrete" mode, this is the maximum score for which an individual star is in the "half-full state".
 */
@property (assign, nonatomic) CGFloat maximumHalfFullAmount;

@end


@protocol XXXStarColorStyle <NSObject>

/**
 *  Color to use to tint the filled star images in a normal state.
 */
@property (strong, nonatomic) UIColor *filledStarColor;
/**
 *  Color to use to tint the filled star images in a highlighted state.
 */
@property (strong, nonatomic) UIColor *filledStarHighlightColor;
/**
 *  Color to use to tint the empty star images in a normal state.
 */
@property (strong, nonatomic) UIColor *emptyStarColor;
/**
 *  Color to use to tint the empty star images in a highlighted state.
 */
@property (strong, nonatomic) UIColor *emptyStarHighlightColor;

@end


@protocol XXXStarImageGroup <NSObject>
/**
 *  Image to use for the filled stars in a normal state.
 */
@property (strong, nonatomic) UIImage *filledStarImage;
/**
 *  Image to use for the filled stars in a highlighted state.
 */
@property (strong, nonatomic) UIImage *filledStarHighlightImage;
/**
 *  Image to use for the empty stars in a normal state.
 */
@property (strong, nonatomic) UIImage *emptyStarImage;
/**
 *  Image to use for the empty stars in a highlighted state.
 */
@property (strong, nonatomic) UIImage *emptyStarHighlightImage;

@end
