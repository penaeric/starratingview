//
//  XXXStarRatingStarView_Internals.h
//  XXXCoreInterface
//

#import "XXXStarRatingStarView.h"

@interface XXXStarRatingStarView ()

@property (assign, nonatomic) XXXStarRatingViewStyle starViewStyle;
@property (assign, nonatomic) CGFloat minimumHalfFullAmount;
@property (assign, nonatomic) CGFloat maximumHalfFullAmount;

// -------------------------------------------------------------------------- //
/*!
 * @methodgroup Display-Style Properties - Colors
 */
// -------------------------------------------------------------------------- //

/*! Color to use to tint the filled star images in a normal state. */
@property (strong, nonatomic) IBInspectable UIColor *filledStarColor;

/*! Color to use to tint the filled star images in a highlighted state. */
@property (strong, nonatomic) IBInspectable UIColor *filledStarHighlightColor;

/*! Color to use to tint the empty star images in a normal state. */
@property (strong, nonatomic) IBInspectable UIColor *emptyStarColor;

/*! Color to use to tint the empty star images in a highlighted state. */
@property (strong, nonatomic) IBInspectable UIColor *emptyStarHighlightColor;

// -------------------------------------------------------------------------- //
/*!
 * @methodgroup Display-Style Properties - Images
 */
// -------------------------------------------------------------------------- //

/*! Image to use for the filled stars in a normal state. */
@property (strong, nonatomic) IBInspectable UIImage *filledStarImage;

/*! Image to use for the filled stars in a highlighted state. */
@property (strong, nonatomic) IBInspectable UIImage *filledStarHighlightImage;

/*! Image to use for the empty stars in a normal state. */
@property (strong, nonatomic) IBInspectable UIImage *emptyStarImage;

/*! Image to use for the empty stars in a highlighted state. */
@property (strong, nonatomic) IBInspectable UIImage *emptyStarHighlightImage;

@end
