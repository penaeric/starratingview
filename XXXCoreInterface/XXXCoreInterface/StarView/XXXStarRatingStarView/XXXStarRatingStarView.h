//
//  XXXStarRatingStarView.h
//  XXXCoreInterface
//

@import UIKit;
#import "XXXStarRatingViewStyle.h"

@interface XXXStarRatingStarView : UIView

- (void)setRating:(CGFloat)rating;

- (void)setHighlighted:(BOOL)highlighted;

/**
 *  @param filledStarImage          Image to use for the filled stars in a normal state.
 *  @param filledStarHighlightImage Image to use for the filled stars in a highlighted state.
 *  @param emptyStarImage           Image to use for the empty stars in a normal state.
 *  @param emptyStarHighlightImage  Image to use for the empty stars in a highlighted state.
 */
- (void)configureWithFilledStarImage:(UIImage *)filledStarImage
            filledStarHighlightImage:(UIImage *)filledStarHighlightImage
                      emptyStarImage:(UIImage *)emptyStarImage
             emptyStarHighlightImage:(UIImage *)emptyStarHighlightImage;

/**
 *  @param filledStarColor          Color to use to tint the filled star images in a normal state.
 *  @param filledStarHighlightColor Color to use to tint the filled star images in a highlighted state.
 *  @param emptyStarColor           Color to use to tint the empty star images in a normal state.
 *  @param emptyStarHighlightColor  Color to use to tint the empty star images in a highlighted state.
 */
- (void)configureWithFilledStarColor:(UIColor *)filledStarColor
            filledStarHighlightColor:(UIColor *)filledstarHighlightColor
                      emptyStarColor:(UIColor *)emptyStarColor
             emptyStarHighlightColor:(UIColor *)emptyStarHighlightColor;

/**
 *  @param viewStyle             The display style it should use for showing its scores.
 *  @param minimumHalfFullAmount A value in [0.0, 1.0]. In the "discrete" mode, this is the minimum score an individual star needs to go to the "half-full state".
 *  @param maximumHalfFullAmount A value in [0.0, 1.0]. In the "discrete" mode, this is the maximum score for which an individual star is in the "half-full state".
 */
- (void)configureWithDisplayStyle:(XXXStarRatingViewStyle)viewStyle
            minimumHalfFullAmount:(CGFloat)minimumHalfFullAmount
            maximumHalfFullAmount:(CGFloat)maximumHalfFullAmount;

@end
