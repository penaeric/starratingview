//
//  XXXStarRatingStarView.m
//  XXXCoreInterface
//

@import XXXCoreUtilities;
#import "XXXStarRatingStarView_Internals.h"

@interface XXXStarRatingStarView ()

@property (nonatomic, assign) IBInspectable CGFloat inspectableRating;

@property (strong, nonatomic) UIImageView *filledImageView;
@property (strong, nonatomic) UIImageView *emptyImageView;

@property (strong, nonatomic) UIImage *currentStarImage;

@property (assign, nonatomic) CGFloat currentRating;
@property (assign, nonatomic, getter=isHighlighted) BOOL highlighted;

@property (strong, nonatomic) UIColor *currentFilledStarColor;
@property (strong, nonatomic) UIColor *currentFilledStarHighlightColor;
@property (strong, nonatomic, readonly) UIColor *effectiveFilledStarColor;
@property (strong, nonatomic) UIColor *currentEmptyStarColor;
@property (strong, nonatomic) UIColor *currentEmptyStarHighlightColor;
@property (strong, nonatomic, readonly) UIColor *effectiveEmptyStarColor;

@end

@implementation XXXStarRatingStarView

#pragma mark - Init

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupViewsIfNeeded];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self setupViewsIfNeeded];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupViewsIfNeeded];
}

#pragma mark -

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.emptyImageView.frame = self.bounds;
    self.filledImageView.frame = CGRectMake(0.0,
                                            0.0,
                                            [self filledStarWidth],
                                            CGRectGetHeight(self.bounds));
}

- (CGFloat)filledStarWidth {
    CGFloat width = CGRectGetWidth(self.bounds) * self.currentRating;
    if (self.starViewStyle == XXXStarRatingViewStyleDiscrete) {
        if (self.currentRating < self.minimumHalfFullAmount) {
            width = 0;
        } else if (self.currentRating > self.maximumHalfFullAmount) {
            width = CGRectGetWidth(self.bounds);
        } else {
            width = CGRectGetWidth(self.bounds) / 2;
        }
    }
    return width;
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    if (!CGRectEqualToRect(frame, self.frame)) {
        [self setNeedsLayout];
    }
}

- (void)setBounds:(CGRect)bounds {
    [super setBounds:bounds];
    if (!CGRectEqualToRect(bounds, self.bounds)) {
        [self setNeedsLayout];
    }
}

- (id)debugQuickLookObject {
    return self.filledImageView.image ?: self.emptyImageView.image;
}

- (CGSize)intrinsicContentSize {
    return self.filledImageView.image.size;
}

- (UIColor *)effectiveFilledStarColor {
    return (self.isHighlighted && self.currentFilledStarHighlightColor) ? self.currentFilledStarHighlightColor : self.currentFilledStarColor;
}

- (UIColor *)effectiveEmptyStarColor {
    return (self.isHighlighted && self.currentEmptyStarHighlightColor) ? self.currentEmptyStarHighlightColor : self.currentEmptyStarColor;
}

#pragma mark - IBInspectable setters

- (void)setInspectableRating:(CGFloat)inspectableRating {
    if (inspectableRating != self.currentRating) {
        self.currentRating = inspectableRating;
        [self setNeedsLayout];
    }
}

- (void)setStarImage:(UIImage *)starImage {
    self.currentStarImage = starImage;
}

#pragma mark - Color properties

- (void)setFilledStarColor:(UIColor *)filledStarColor {
    if (![filledStarColor isEqual:self.currentFilledStarColor]) {
        self.currentFilledStarColor = filledStarColor;
        [self syncColorProperties];
    }
}

- (void)setFilledStarHighlightColor:(UIColor *)filledStarHighlightColor {
    if (![filledStarHighlightColor isEqual:self.currentFilledStarHighlightColor]) {
        self.currentFilledStarHighlightColor = filledStarHighlightColor;
        [self syncColorProperties];
    }
}

- (void)setEmptyStarColor:(UIColor *)emptyStarColor {
    if (![emptyStarColor isEqual:self.currentEmptyStarColor]) {
        self.currentEmptyStarColor = emptyStarColor;
        [self syncColorProperties];
    }
}

- (void)setEmptyStarHighlightColor:(UIColor *)emptyStarHighlightColor {
    if (![emptyStarHighlightColor isEqual:self.currentEmptyStarHighlightColor]) {
        self.currentEmptyStarHighlightColor = emptyStarHighlightColor;
        [self syncColorProperties];
    }
}

#pragma mark - Image properties

@dynamic filledStarImage;
- (UIImage *)filledStarImage {
    return self.filledImageView.image;
}

- (void)setFilledStarImage:(UIImage *)filledStarImage {
    if (!XXXEquivalentObjects(filledStarImage, self.filledStarImage)) {
        self.filledImageView.image = filledStarImage;
    }
}

@dynamic filledStarHighlightImage;
- (UIImage *)filledStarHighlightImage {
    return self.filledImageView.highlightedImage;
}

- (void)setFilledStarHighlightImage:(UIImage *)filledStarHighlightImage {
    if (!XXXEquivalentObjects(filledStarHighlightImage, self.filledStarHighlightImage)) {
        self.filledImageView.highlightedImage = filledStarHighlightImage;
    }
}

@dynamic emptyStarImage;
- (UIImage *)emptyStarImage {
    return self.emptyImageView.image;
}

- (void)setEmptyStarImage:(UIImage *)emptyStarImage {
    if (!XXXEquivalentObjects(emptyStarImage, self.emptyStarImage)) {
        self.emptyImageView.image = emptyStarImage;
    }
}

@dynamic emptyStarHighlightImage;
- (UIImage *)emptyStarHighlightImage {
    return self.emptyImageView.highlightedImage;
}

- (void)setEmptyStarHighlightImage:(UIImage *)emptyStarHighlightImage {
    if (!XXXEquivalentObjects(emptyStarHighlightImage, self.emptyStarHighlightImage)) {
        self.emptyImageView.highlightedImage = emptyStarHighlightImage;
    }
}

#pragma mark -

- (void)setupViewsIfNeeded {
    if (self.emptyImageView == nil) {
        self.starViewStyle = XXXStarRatingViewStyleContinuous;
        
        self.emptyImageView = [[UIImageView alloc] init];
        self.emptyImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.emptyImageView.clipsToBounds = YES;
        self.emptyImageView.contentMode = UIViewContentModeRight;
        [self addSubview:self.emptyImageView];
    }
    if (self.filledImageView == nil) {
        self.filledImageView = [[UIImageView alloc] init];
        self.filledImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.filledImageView.clipsToBounds = YES;
        self.filledImageView.contentMode = UIViewContentModeLeft;
        [self addSubview:self.filledImageView];
    }
    [self bringSubviewToFront:self.filledImageView];
}

- (void)syncColorProperties {
    self.filledImageView.tintColor = self.effectiveFilledStarColor;
    self.emptyImageView.tintColor = self.effectiveEmptyStarColor;
    [self setNeedsDisplay];
}

#pragma mark - Public API

- (void)configureWithFilledStarImage:(UIImage *)filledStarImage
            filledStarHighlightImage:(UIImage *)filledStarHighlightImage
                      emptyStarImage:(UIImage *)emptyStarImage
             emptyStarHighlightImage:(UIImage *)emptyStarHighlightImage {
    [self setupViewsIfNeeded];
    self.filledStarImage = [filledStarImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.filledStarHighlightImage = [filledStarHighlightImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.emptyStarImage = [emptyStarImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.emptyStarHighlightImage = [emptyStarHighlightImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self setNeedsLayout];
}

- (void)configureWithFilledStarColor:(UIColor *)filledStarColor
            filledStarHighlightColor:(UIColor *)filledstarHighlightColor
                      emptyStarColor:(UIColor *)emptyStarColor
             emptyStarHighlightColor:(UIColor *)emptyStarHighlightColor {
    [self setupViewsIfNeeded];
    self.currentFilledStarColor = filledStarColor;
    self.currentFilledStarHighlightColor = filledstarHighlightColor;
    self.currentEmptyStarColor = emptyStarColor;
    self.currentEmptyStarHighlightColor = emptyStarHighlightColor;
    [self syncColorProperties];
}

- (void)setRating:(CGFloat)rating {
    self.currentRating = rating;
    [self setNeedsLayout];
}

- (void)setHighlighted:(BOOL)highlighted {
    _highlighted = highlighted;
    [self.filledImageView setHighlighted:highlighted];
    [self.emptyImageView setHighlighted:highlighted];
    [self syncColorProperties];
}

- (void)configureWithDisplayStyle:(XXXStarRatingViewStyle)viewStyle
            minimumHalfFullAmount:(CGFloat)minimumHalfFullAmount
            maximumHalfFullAmount:(CGFloat)maximumHalfFullAmount {
    BOOL needsUpdate = NO;
    
    if (viewStyle != self.starViewStyle) {
        self.starViewStyle = viewStyle;
        needsUpdate = YES;
    }
    
    if (minimumHalfFullAmount != self.minimumHalfFullAmount) {
        self.minimumHalfFullAmount = minimumHalfFullAmount;
        if (self.starViewStyle == XXXStarRatingViewStyleDiscrete) {
            needsUpdate = YES;
        }
    }
    
    if (maximumHalfFullAmount != self.maximumHalfFullAmount) {
        self.maximumHalfFullAmount = maximumHalfFullAmount;
        if (self.starViewStyle == XXXStarRatingViewStyleDiscrete) {
            needsUpdate = YES;
        }
    }
    
    if (needsUpdate) {
        [self setNeedsLayout];
    }
}

@end
